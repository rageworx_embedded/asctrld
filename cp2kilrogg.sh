#!/bin/bash

#default source path is for DEBUG.

SRCPATH="bin/Release"
SRCFILE="asctrld"
DSTPATH="$HOME/Mozart/kilrogg/usr/rootfs/usr/sbin"

#Checking source path and file.

if [ ! -e $SRCPATH ]; then
    echo "Source path not found: $SRCPATH"
    exit 0
fi

SRCCOMPATH="$SRCPATH/$SRCFILE"

if [ ! -e $SRCCOMPATH ]; then
    echo "Compiled binary seems not be existed."
    exit 0
fi

if [ ! -e $DSTPATH ]; then
    echo "Destination path not found: $DSTPATH"
    exit 0
fi

#Copying file...

echo "Copying file $SRCFILE to $DSTPATH ..."

cp -rf $SRCCOMPATH $DSTPATH

echo "done."
