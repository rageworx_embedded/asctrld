#ifndef __AS_WIFI_INFO_H__
#define __AS_WIFI_INFO_H__

enum wifi_types
{
    WIFI_T_NONE = 0, /// == OPEN
    WIFI_T_OPEN,
    WIFI_T_WEP,
    WIFI_T_WPATKIP,
    WIFI_T_WPAAES,
    WIFI_T_WPA2TKIP,
    WIFI_T_WPA2AES,
    WIFI_T_WPA2BOTH  /// == AES + TKIP
};

#define MAX_WIFI_INTERFACE_NAME_LEN     128
#define MAX_WIFI_SSID_LEN               128
#define MAX_WIFI_ITEM_LEN               32
#define MAX_WIFI_PSK_LEN                16

typedef struct _wifi_info
{
    int     wifi_type;

    char    interface[MAX_WIFI_INTERFACE_NAME_LEN];
    bool    update_config;

    char    net_ssid[MAX_WIFI_SSID_LEN];
    char    net_keymgmt[MAX_WIFI_ITEM_LEN];
    char    net_pairwise[MAX_WIFI_ITEM_LEN];
    char    net_proto[MAX_WIFI_ITEM_LEN];
    char    net_group[MAX_WIFI_ITEM_LEN];
    char    net_psk[MAX_WIFI_PSK_LEN];
}wifi_info;

int new_wifi_info( int wtype = WIFI_T_NONE );
int load_wifi_info( const char* rpath = NULL );
int change_wifi_info( int new_wtype, const char* ssid, const char* psk );
int save_wifi_info();
int get_current_wifi_info( wifi_info* winfo );

#endif ///__AS_WIFI_INFO_H__
