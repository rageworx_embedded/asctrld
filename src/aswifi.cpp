#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "aswifi.h"

////////////////////////////////////////////////////////////////////////////////

#define WPA_SUPPLICANT_IF_NAME      "/var/run/wpa_supplicant"
#define WPA_SUPPLICANT_CONF_PATH    "/etc/wpa_supplicant.conf"

////////////////////////////////////////////////////////////////////////////////

static wifi_info    w_info;
static bool         w_loaded = false;
static char         wpa_fn[256] = {0};

////////////////////////////////////////////////////////////////////////////////

void strtrim( char* &o, const char* s )
{
    if ( s == NULL )
        return;

    int szs = strlen( s );
    int szo = 0;

    o = (char*)calloc( 1, szs );

    if ( o != NULL )
    {
        for( int cnt=0; cnt<szs; cnt++ )
        {
            if ( ( s[cnt] != 0x20 ) &&
                 ( s[cnt] != '\t' ) &&
                 ( s[cnt] != '\n' ) &&
                 ( s[cnt] != '\f' ) )
            {
                o[szo] = s[cnt];
                szo++;
            }
        }
    }
}

void splitval( const char* refstr, char* &o1, char* &o2 )
{
    if ( refstr == NULL )
        return;

    char* tmpstr = NULL;

    strtrim( tmpstr, refstr );

    if ( tmpstr != NULL )
    {
        char* stok = strtok( tmpstr, "=" );
        int   scnt = 0;
        while( stok != NULL )
        {
            if ( scnt == 0 )
            {
                o1 = strdup( stok );
            }
            else
            if ( scnt == 1 )
            {
                o2 = strdup( stok );
            }

            stok = strtok( NULL, "=" );
            scnt++;
        }
    }

    free( tmpstr );
}

void reset_w_info( int wtype )
{
    strcpy( w_info.interface, WPA_SUPPLICANT_IF_NAME );
    w_info.update_config = 1;

    memset( w_info.net_ssid, 0, MAX_WIFI_SSID_LEN );
    memset( w_info.net_keymgmt, 0, MAX_WIFI_ITEM_LEN );
    memset( w_info.net_pairwise, 0, MAX_WIFI_ITEM_LEN );
    memset( w_info.net_proto, 0, MAX_WIFI_ITEM_LEN );
    memset( w_info.net_group, 0, MAX_WIFI_ITEM_LEN );
    memset( w_info.net_psk, 0, MAX_WIFI_PSK_LEN );

    w_info.wifi_type = wtype;

    switch( wtype )
    {
        case WIFI_T_OPEN:
            strcpy( w_info.net_proto, "WEP" );
            break;

        case WIFI_T_WPATKIP:
            strcpy( w_info.net_keymgmt, "WPA-PSK" );
            strcpy( w_info.net_pairwise, "TKIP" );
            strcpy( w_info.net_group, "TKIP" );
            strcpy( w_info.net_proto, "WPA" );
            break;

        case WIFI_T_WPAAES:
            strcpy( w_info.net_keymgmt, "WPA-PSK" );
            strcpy( w_info.net_pairwise, "CCMP" );
            strcpy( w_info.net_group, "CCMP" );
            strcpy( w_info.net_proto, "WPA" );
            break;

        case WIFI_T_WPA2TKIP:
            strcpy( w_info.net_keymgmt, "WPA-PSK" );
            strcpy( w_info.net_pairwise, "TKIP" );
            strcpy( w_info.net_group, "TKIP" );
            strcpy( w_info.net_proto, "RSN" );
            break;

        case WIFI_T_WPA2AES:
            strcpy( w_info.net_keymgmt, "WPA-PSK" );
            strcpy( w_info.net_pairwise, "CCMP" );
            strcpy( w_info.net_group, "CCMP" );
            strcpy( w_info.net_proto, "RSN" );
            break;

        case WIFI_T_WPA2BOTH:
            strcpy( w_info.net_keymgmt, "WPA-PSK" );
            strcpy( w_info.net_pairwise, "CCMP TKIP" );
            strcpy( w_info.net_group, "CCMP TKIP" );
            strcpy( w_info.net_proto, "WPA RSN" );
            break;
    }
}

int new_wifi_info( int wtype )
{
    if ( w_loaded == true )
    {
        w_loaded = false;
    }

    reset_w_info( wtype );

    return 0;
}

int load_wifi_info( const char* rpath )
{
    if ( rpath == NULL )
    {
        strcpy( wpa_fn, WPA_SUPPLICANT_CONF_PATH );
    }
    else
    {
        strcpy( wpa_fn, rpath );
    }

    if ( access( wpa_fn, F_OK ) == 0 )
    {
        FILE* fpConf = fopen( wpa_fn, "r" );
        if ( fpConf != NULL )
        {
            char curln[80];
            bool netflag = false;
            int  itmcnt = 0;

            while ( !feof( fpConf ) )
            {
                if ( fgets( curln, 80, fpConf ) != NULL )
                {
                    char* nm = NULL;
                    char* vl = NULL;

                    splitval( curln, nm, vl );

                    int sznm = 0;

                    if ( nm != NULL )
                    {
                        sznm = strlen( nm );
                    }

                    int szvl = 0;

                    if ( vl != NULL )
                    {
                        szvl = strlen( vl );
                    }

                    if ( sznm > 0 )
                    {
                        if ( szvl > 0 )
                        {
                            if ( strcmp( nm, "ctrl_interface" ) == 0 )
                            {
                                strcpy(w_info.interface, vl);
                                itmcnt++;
                            }
                            else
                            if ( strcmp( nm, "update_config" ) == 0 )
                            {
                                int ivl = atoi( vl );

                                if ( ivl > 0 )
                                {
                                    w_info.update_config = true;
                                }
                                else
                                {
                                    w_info.update_config = false;
                                }

                                itmcnt++;
                            }
                            else
                            if ( strcmp( nm, "network" ) == 0 )
                            {
                                if ( strcmp( vl, "{" ) == 0 )
                                {
                                    netflag = true;
                                }
                            }
                            else
                            if ( ( strcmp( nm, "ssid" ) == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_ssid, vl );
                                itmcnt++;
                            }
                            else
                            if ( ( strcmp( nm, "key_mgmt" ) == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_keymgmt, vl );
                                itmcnt++;
                            }
                            else
                            if ( ( strcmp( nm, "psk" ) == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_psk, vl );
                                itmcnt++;
                            }
                            else
                            if ( ( strcmp ( nm, "pairwise") == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_pairwise, vl );
                                itmcnt++;
                            }
                            else
                            if ( ( strcmp ( nm, "proto" ) == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_proto, vl );
                                itmcnt++;
                            }
                            else
                            if ( ( strcmp ( nm, "group" ) == 0 ) && ( netflag == true ) )
                            {
                                strcpy( w_info.net_group, vl );
                                itmcnt++;
                            }
                        }
                        else
                        if ( strcmp ( nm, "}" ) == 0 )
                        {
                            netflag = false;
                        }
                    }

                    if ( nm != NULL )
                    {
                        free( nm );
                    }

                    if ( vl != NULL )
                    {
                        free( vl );
                    }

                }
            } /// end of while()

            fclose( fpConf );

            return itmcnt;
        }
    }

    return 0;
}

int change_wifi_info( int new_wtype, const char* ssid, const char* psk )
{
    if ( new_wtype <= WIFI_T_NONE )
        return -2;

    int szssid = 0;
    int szpsk = 0;

    if ( new_wtype > WIFI_T_OPEN )
    {
        if ( ( psk == NULL ) || ( ssid == NULL ) )
            return -3;

        szssid = strlen( ssid );
        if ( szssid > MAX_WIFI_SSID_LEN )
            return -4;

        szpsk = strlen( psk );

        if ( szpsk > MAX_WIFI_PSK_LEN )
            return -5;
    }

    reset_w_info( new_wtype );

    switch( new_wtype )
    {
        case WIFI_T_WPATKIP:
        case WIFI_T_WPAAES:
        case WIFI_T_WPA2TKIP:
        case WIFI_T_WPA2AES:
        case WIFI_T_WPA2BOTH:
            {
                strcpy( w_info.net_ssid, ssid );
                strcpy( w_info.net_psk, psk );
            }
            break;
    }

    return 0;
}

int save_wifi_info()
{
    if ( strlen( wpa_fn ) == 0 )
        return -1;

    if ( access( wpa_fn, F_OK) == 0 )
    {
        if ( unlink( wpa_fn ) != 0 )
        {
            perror( "Cannot access WiFi configuration file." );
            return -1;
        }
    }

    system("sync");

    FILE* fpConf = fopen( wpa_fn, "w" );
    if ( fpConf != NULL )
    {
        fprintf( fpConf, "ctrl_interface=%s\n", w_info.interface );
        fprintf( fpConf, "update_config=%d\n", (int)w_info.update_config );
        fprintf( fpConf, "\n" );
        fprintf( fpConf, "network={\n");

        if ( strlen( w_info.net_ssid ) > 0 )
        {
            fprintf( fpConf, "\tssid=\"%s\"\n", w_info.net_ssid );
        }

        if ( strlen( w_info.net_proto ) > 0 )
        {
            fprintf( fpConf, "\tproto=%s\n", w_info.net_proto );
        }

        if ( strlen( w_info.net_keymgmt ) > 0 )
        {
            fprintf( fpConf, "\tkey_mgmt=%s\n", w_info.net_keymgmt );
        }

        if ( strlen( w_info.net_psk ) > 0 )
        {
            fprintf( fpConf, "\tpsk=\"%s\"\n", w_info.net_psk );
        }

        if ( strlen( w_info.net_pairwise ) > 0 )
        {
            fprintf( fpConf, "\tpairwise=%s\n", w_info.net_pairwise );
        }

        if ( strlen( w_info.net_group ) > 0 )
        {
            fprintf( fpConf, "\tgroup=%s\n", w_info.net_group );
        }

        fprintf( fpConf,"}\n" );
        fclose( fpConf );

        system("sync");
    }

    return 0;
}

int get_current_wifi_info( wifi_info* winfo )
{
    if ( winfo != NULL )
    {
        winfo->wifi_type = w_info.wifi_type;
        winfo->update_config = w_info.update_config;

        strcpy( winfo->interface,     w_info.interface );
        strcpy( winfo->net_ssid,      w_info.net_ssid );
        strcpy( winfo->net_keymgmt,   w_info.net_keymgmt );
        strcpy( winfo->net_psk,       w_info.net_psk );
        strcpy( winfo->net_pairwise,  w_info.net_pairwise );
        strcpy( winfo->net_proto,     w_info.net_proto );
        strcpy( winfo->net_group,     w_info.net_group );

        return 0;
    }

    return -1;
}
