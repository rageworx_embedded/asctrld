////////////////////////////////////////////////////////////////////////////////
// AnyStream main control daemon.
// ---------------------------------------------------------------------------
// (C)2016,2016 Raphael Kim @ 3Iware
//
//  reversion 0.3.6.xx
//
//
////////////////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>

#include <time.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <pthread.h>

#include "proclist.h"
#include "aswifi.h"

#include <fcntl.h>
#include <sys/ioctl.h>
// Anystreaming LED IO definition
#include "gpio_export.h"

#define VER_STR             "0.4.1.102"

#define DEF_SZ_READ         64 * 1024   /// 64K
#define DEF_SZ_WRITE        DEF_SZ_READ
#define DEF_PORT_TCP        8801
#define DEF_LOG_MAX_SIZE    32 * 1024   /// 32K
#define DEF_PORT_FW_TCP     8900

#define GPIO_LED_RED        "/dev/gpio5"
#define GPIO_LED_GREEN      "/dev/gpio12"
#define GPIO_LED_BLUE       "/dev/gpio13"

#define ETC_VERSION_F       "/etc/bdversion"
#define ETC_TARGZ_F         "/usr/local/firmware/default_flashfs_files.tar.gz"

#define DEF_AS_SFTPD_BIN    "as_sftpd"
#define DEF_AS_SFTPD_PARAM  "-f /etc/sftpd.conf"

#define DEF_AS_FW_FILE      "/var/fwupgrade.zip"
#define DEF_AS_FW_WRITER    "as_fw_writer"
#define DEF_AS_FW_RESULT    "/var/fwupgrade_result"

////////////////////////////////////////////////////////////////////////////////

enum D_WORK_TYPES
{
    D_WORK_NONE = 0,

    D_WORK_GETMACADDR_ETH,
    D_WORK_GETMACADDR_WLAN,

    D_WORK_GETIPADDR_ETH,
    D_WORK_GETIPADDR_WLAN,

    D_WORK_GETSTATE,

    D_WORK_SETIPADDR_ETH,   /// Option comes with MAC address.
    D_WORK_SETIPADDR_WLAN,  /// Option comes with MAC address.

    D_WORK_SETWIFI_OFF,
    D_WORK_SETWIFI_AP,
    D_WORK_SETWIFI_CLI,

    D_WORK_FACTORY_RESET,

    D_WORK_FW_UPGRADE_TOGGLE,
    D_WORK_FW_UPGRADE_START,

    D_WORK_MAX
};

typedef struct _D_WORK_PARAM
{
    int     wtype;
    int     clisockfd;
    char*   param1;
    char*   param2;
    char*   param3;
}D_WORK_PARAM;

////////////////////////////////////////////////////////////////////////////////

const int   proclists  = 6;
const char* proclist[] =
{
    "as_irda",
    "as_hid",
    "udpctrl",
    "aenc",
    "venc",
    "rtsps"
};

const char proc_led_app[]       = "asledon";
const char proc_wifi_drv[]      = "xnwifi";
const char proc_wifi_cli[]      = "wpa_supplicant";
const char proc_wifi_ap[]       = "hostapd";
const char proc_wifi_dhcpd[]    = "udhcpd";
const char proc_wifi_dhcpc[]    = "udhcpc";

////////////////////////////////////////////////////////////////////////////////

static int          sockfd_udp_svr;
static int          sockfd_udp_cli;
static int          sock_udp_clilen;
static int          sock_udp_state;
struct sockaddr_in  server_udp_addr;
struct sockaddr_in  server_udp_addr_ex;

static int          server_fdsock = -1;
static int          client_fdsock = -1;
static int          fdsock;
static int          fd_num;

static int          clients[FD_SETSIZE] = {-1};
static int          clients_max;
static socklen_t    client_len;

static char         sock_readbuff[DEF_SZ_READ];

static fd_set       readfds;
static fd_set       allfds;

static bool         rcv_blocked = false;

static int          fd_led_red = -1;
static int          fd_led_green = -1;
static int          fd_led_blue = -1;

static bool         sys_need_reboot = false;

// A simple mutex for send2clients().
static bool         send2clients_inuse = false;


struct sockaddr_in clientaddr;
struct sockaddr_in serveraddr;

pthread_t           pt_id_read;
pthread_t           pt_id_tcp;
pthread_t           pt_id_netwatch;
pthread_t           pt_id_oncework;

int                 pt_nm;
bool                pt_lived = true;

bool                cmdmtx  = false;
char                udp_recvbuff[DEF_SZ_READ] = {0};

bool                flag_quit = false;
char*               me = NULL;

char*               logBuff = NULL;
unsigned int        logSz   = 0;
unsigned int        logQue  = 0;
bool                logInUse = false;

bool                opt_wifi = true;
unsigned long long  clients_cnt = 0;
char                version_full_str[128] = {0};

////////////////////////////////////////////////////////////////////////////////

void* pthread_loop_udp( void* p );
void* pthread_loop_socket_tcp( void* p );
void* pthread_loop_netwatch( void* p );
void* pthread_once_proc( void* p );

////////////////////////////////////////////////////////////////////////////////

void safe_usleep( long usec )
{
    struct timeval tv = {0};
    tv.tv_sec  = usec / 1000000L;
    tv.tv_usec = usec % 1000000L;
    select( 0, 0, 0, 0, &tv );
}

void clear_log()
{
    while( logInUse == true )
    {
        safe_usleep( 50 * 1000 );
    }

    logInUse = true;

    if ( logBuff != NULL )
    {
        memset( logBuff, 0, logSz );
        logQue = 0;
    }

    logInUse = false;
}

void alloc_log()
{
    if ( logBuff == NULL )
    {
        logBuff = (char*)malloc( DEF_LOG_MAX_SIZE );
        if ( logBuff != NULL )
        {
            logSz = DEF_LOG_MAX_SIZE;
            clear_log();
        }
    }
}

void release_log()
{
    while( logInUse == true )
    {
        safe_usleep( 50 * 1000 );
    }

    logInUse = true;

    if ( logBuff != NULL )
    {
        logSz = 0;
        free( logBuff );
        logBuff = NULL;
    }

    logInUse = false;
}

void add_log( const char* s )
{
    if ( ( logBuff != NULL ) && ( logInUse == false ) )
    {
        logInUse = true;

        char mappstr[128] = {0};

        time_t      cur_time = time( NULL );
        struct tm*  cur_tm   = localtime( &cur_time );

        sprintf( mappstr,
                 "[%02d:%02d:%02d]%s\n",
                 cur_tm->tm_hour,
                 cur_tm->tm_min,
                 cur_tm->tm_sec,
                 s );

        int mppstrsz = strlen( mappstr );
        if ( mppstrsz > 0 )
        {
            if ( ( mppstrsz + logQue ) > logSz )
            {
                clear_log();
            }

            memcpy( &logBuff[ logQue ], mappstr, mppstrsz );
            logQue += mppstrsz;
        }
#ifdef DEBUG
        printf("%s", mappstr);
#endif // DEBUG
        logInUse = false;
    }
}

void load_all_versions()
{
    memset( version_full_str, 0, 128 );

    sprintf( version_full_str, "asctrldversion=%s\n", VER_STR );

    FILE* fp = fopen( ETC_VERSION_F, "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0, SEEK_END );
        int fsz = ftell( fp );
        fseek( fp, 0, SEEK_SET );

        if ( fsz > 0 )
        {
            char* tmpbuff = new char[ fsz + 1 ];
            if ( tmpbuff != NULL )
            {
                memset( tmpbuff, 0 , fsz + 1 );

                fread( tmpbuff, 1, fsz, fp );

                strncat( version_full_str, tmpbuff, 64 );

                free( tmpbuff );
            }
        }

        fclose( fp );
    }
}

bool open_led_devs()
{

    fd_led_blue = open( GPIO_LED_BLUE, O_RDWR );
    if ( fd_led_blue >= 0 )
    {
        ioctl( fd_led_blue, GPIO_PINDIR_OUT );
    }

    fd_led_green = open( GPIO_LED_GREEN, O_RDWR );
    if ( fd_led_green >= 0 )
    {
        ioctl( fd_led_green, GPIO_PINDIR_OUT );
    }

    fd_led_red = open( GPIO_LED_RED, O_RDWR );
    if ( fd_led_red >= 0 )
    {
        ioctl( fd_led_red, GPIO_PINDIR_OUT );
    }

#ifdef DEBUG
    printf( "fd_led_red = %d\n", fd_led_red );
    printf( "fd_led_green = %d\n", fd_led_green );
    printf( "fd_led_blue = %d\n", fd_led_blue );
#endif // DEBUG

    if ( ( fd_led_red >= 0 ) && ( fd_led_green >= 0 ) && ( fd_led_blue >= 0 ) )
    {
        return true;
    }

    return false;
}

void ctrl_set_led( bool red, bool green, bool blue )
{
    if ( red == true )
    {
        if ( fd_led_red >= 0 )
        {
            ioctl( fd_led_red, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_red >= 0 )
        {
            ioctl( fd_led_red, GPIO_DATA_SET );
        }
    }

    if ( green == true )
    {
        if ( fd_led_green >= 0 )
        {
            ioctl( fd_led_green, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_green >= 0 )
        {
            ioctl( fd_led_green, GPIO_DATA_SET );
        }
    }

    if ( blue == true )
    {
        if ( fd_led_blue >= 0 )
        {
            ioctl( fd_led_blue, GPIO_DATA_CLEAR );
        }
    }
    else
    {
        if ( fd_led_blue >= 0 )
        {
            ioctl( fd_led_blue, GPIO_DATA_SET );
        }
    }
}

void close_led_devs()
{
    ctrl_set_led( false, false, false );

    if ( fd_led_red >= 0 )
    {
        close( fd_led_red );
        fd_led_red = -1;
    }

    if ( fd_led_green >= 0 )
    {
        close( fd_led_green );
        fd_led_green = -1;
    }

    if ( fd_led_blue >= 0 )
    {
        close( fd_led_blue );
        fd_led_blue = -1;
    }
}

bool open_sock_udp()
{
    const short port_svr = 5002;
    const short port_cli = 5008;

#ifdef DEBUG
    printf("... openning UDP #1 (write, lo, port = %d) : ", port_svr);
#endif /// of DEBUG

    sock_udp_clilen = sizeof(server_udp_addr);
    sockfd_udp_svr = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd_udp_svr < 0)
    {
#ifdef DEBUG
        printf("Failure(socket error)\n");
#endif
        return false;
    }

    bzero(&server_udp_addr, sizeof(server_udp_addr));
    server_udp_addr.sin_family = AF_INET;
    server_udp_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);    /// loopback.
    server_udp_addr.sin_port = htons(port_svr);

#ifdef DEBUG
    printf("Ok.\n");
    printf("... configure loop back multicast : ");
#endif

    int do_enable = 1;
    int reti = setsockopt(sockfd_udp_svr, IPPROTO_IP, IP_MULTICAST_LOOP,  & do_enable, sizeof(do_enable));
#ifdef DEBUG
    printf("%d\n", reti);
#endif

    /////////////////////////////////////////////////////////////////

#ifdef DEBUG
    printf("... openning UDP #2 (read, lo, port = %d) : ", port_cli);
#endif // DEBUG

    sock_udp_clilen = sizeof(server_udp_addr);

    sockfd_udp_cli = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd_udp_cli < 0)
    {
#ifdef DEBUG
        printf("Failure(socket error)\n");
#endif /// of DEBUG
        return false;
    }

    bzero(&server_udp_addr_ex, sizeof(server_udp_addr_ex));
    server_udp_addr_ex.sin_family = AF_INET;
    server_udp_addr_ex.sin_addr.s_addr = htonl(INADDR_LOOPBACK);    /// loopback.
    server_udp_addr_ex.sin_port = htons(port_cli);

    int sockstate = bind( sockfd_udp_cli, (struct sockaddr*)&server_udp_addr_ex, sizeof(server_udp_addr_ex) );
    if ( sockstate < 0 )
    {
#ifdef DEBUG
        printf("Error(%d), bind failure.\n", sockstate );
#endif
        return false;
    }

#ifdef DEBUG
    printf("Ok.\n");
    printf("... configure loop back multicast : ");
#endif // DEBUG

    reti = setsockopt(sockfd_udp_cli, IPPROTO_IP, IP_MULTICAST_LOOP,  & do_enable, sizeof(do_enable));
#ifdef DEBUG
    printf("%d\n", reti);
#endif /// of DEBUG

    return true;
}

void close_sock_udp()
{
    if ( sockfd_udp_svr > 0 )
    {
        close( sockfd_udp_svr );
    }

    if ( sockfd_udp_cli > 0 )
    {
        close( sockfd_udp_cli );
    }
}

bool open_sock_tcp()
{
#ifdef DEBUG
    printf("... openning TCP #1 (write, lo, port = %d) : ", DEF_PORT_TCP);
#endif // DEBUG

    if ( (server_fdsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0 )
    {
        perror( "socket error : " );
        return false;
    }

    // Let make TCP server re-use IP address if it has already blocked.
    int optval = 1;
    setsockopt( server_fdsock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );

    // set server to TCP non-block.
    int retfcntl = fcntl( server_fdsock, F_SETFL, O_NONBLOCK );
    if ( retfcntl < 0 )
    {
        perror( "socket cannot be set to non-block:" );
        return false;
    }

    bzero(&serveraddr, sizeof(serveraddr));

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
    serveraddr.sin_port = htons( DEF_PORT_TCP );

    int state = bind (server_fdsock, (struct sockaddr *)&serveraddr, sizeof(serveraddr));

    if (state == -1)
    {
        close( server_fdsock );
        perror("bind error : ");
        return false;
    }

    state = listen(server_fdsock, 5);
    if (state == -1)
    {
        close( server_fdsock );
        perror("listen error : ");
        return false;
    }

#ifdef DEBUG
    printf("OK.\n");
#endif // DEBUG
    return true;
}

bool close_sock_tcp()
{
    for ( int cnt=0; cnt<FD_SETSIZE; cnt++ )
    {
        if (clients[cnt] >= 0)
        {
            close( clients[cnt] );
            clients[cnt] = -1;
        }
    }

    return true;
}

int read_tcp_nb_client( int fds, fd_set* active_fds )
{
    char rd_buff[128]    = {0};
    int  offset          = 0;
    int  sz_rd           = 0;
    int  eof_cnt         = 0;

    while( offset < 2 ) // Minimal 2 bytes !
    {
        memset( rd_buff, 0, 128 );
        sz_rd = read( fds, rd_buff, 128 );

        if( sz_rd < 0 )
        {
            switch( errno )
            {
                case EINTR:
                case EAGAIN:
                    // -- need disconnect.
                    return -3;
                    break;

                case ECONNRESET: /// TCP connection reset !
                    return -1;

                default:
                    return -2;
            }
        }
        else
        if ( sz_rd == 0 ) /// EOF socket.
        {
            eof_cnt++;
            if ( eof_cnt > 10 )
            {
                return offset;
            }
        }

        memcpy( &sock_readbuff[offset], rd_buff, sz_rd );
        offset += sz_rd;
    }

    return offset;
}

int recvfrom_check(int socket, long sec, long usec)
{
	timeval timeout;
	timeout.tv_sec = sec;
	timeout.tv_usec = usec;

	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(socket, &fds);

	int reti = select(0, &fds, 0, 0, &timeout);

	FD_CLR(socket, &fds);

	return reti;
}

bool send2clients( const char* s, unsigned ssz )
{
    // Don't care client get this msg. or not. me alive is best.
    if ( send2clients_inuse == true )
        return false;

    bool retb = false;

    send2clients_inuse = true;

    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
    {
        if ( ( clients[cnt] >= 0 ) && ( clients[cnt] != server_fdsock ) )
        {
            // Just send it, don't care client got or not.
            send( clients[cnt], s, ssz, MSG_NOSIGNAL );

            if ( retb == false )
            {
                retb = true;
            }
        }
    }

    safe_usleep( 50 * 1000 );

    send2clients_inuse = false;

    return retb;
}

bool send2client( int sfd, const char* s, unsigned ssz )
{
    if ( send2clients_inuse == true )
        return false;

    bool retb = false;

    send2clients_inuse = true;

    if ( sfd != server_fdsock )
    {
        send( sfd, s, ssz, MSG_NOSIGNAL );

        if ( retb == false )
        {
            retb = true;
        }
    }

    safe_usleep( 50 * 1000 );

    send2clients_inuse = false;

    return retb;
}

void getoptparams3( const char* refs, char** p1, char** p2, char** p3 )
{
    if ( refs != NULL )
    {
        char* srcs = strdup( refs );

        if ( srcs != NULL )
        {
            char* stok = strtok( srcs, "," );
            int   ncnt = 0;

            while( stok != NULL )
            {
                switch( ncnt )
                {
                    case 0:
                        *p1 = strdup( stok );
                        break;

                    case 1:
                        *p2 = strdup( stok );
                        break;

                    case 2:
                        *p3 = strdup( stok );
                        break;

                }

                stok = strtok( NULL, "," );
                ncnt++;
            }

            free ( srcs );
        }
    }
}

void getoptparams4( const char* refs, char** p1, char** p2, char** p3, char** p4 )
{
    if ( refs != NULL )
    {
        char* srcs = strdup( refs );

        if ( srcs != NULL )
        {
            char* stok = strtok( srcs, "," );
            int   ncnt = 0;

            while( stok != NULL )
            {
                switch( ncnt )
                {
                    case 0:
                        *p1 = strdup( stok );
                        break;

                    case 1:
                        *p2 = strdup( stok );
                        break;

                    case 2:
                        *p3 = strdup( stok );
                        break;

                    case 3:
                        *p4 = strdup( stok );
                        break;
                }

                stok = strtok( NULL, "," );
                ncnt++;
            }

            free ( srcs );
        }
    }
}


bool convert_ipstr_to_iparray( const char* is, unsigned char* ia )
{
    if ( ( is == NULL ) || ( ia == NULL ) )
    {
        return false;
    }

    char* srcs = strdup( is );
    char* stok = strtok( srcs, "." );
    int   ncnt = 0;

    while( stok != NULL )
    {
        unsigned in = atoi( stok );
        ia[ncnt] = in;
        ncnt++;
        if ( ncnt > 3 )
            break;
        stok = strtok( NULL, "." );
    }

    free( srcs );

    return true;
}

int  upow( int n, int m )
{
    int num = n;
    int cnt = 0;

    if ( m == 0 )
        return 0;

    for( cnt=0; cnt<m-1; cnt++ )
    {
        num *= n;
    }

    return num;
}

void uatoh( const char* src, unsigned int* out )
{
    char cnt;
    char c_ch;
    char c_que=0;
    char c_buf[10]={0};

    if ( src != NULL )
    {
        while( *src )
        {
            c_ch = *src;
            if ( c_ch == ' ' )
                break;

            switch ( c_ch )
            {
                case '0': c_ch = 0; break;
                case '1': c_ch = 1; break;
                case '2': c_ch = 2; break;
                case '3': c_ch = 3; break;
                case '4': c_ch = 4; break;
                case '5': c_ch = 5; break;
                case '6': c_ch = 6; break;
                case '7': c_ch = 7; break;
                case '8': c_ch = 8; break;
                case '9': c_ch = 9; break;
                case 'a':
                case 'A': c_ch = 10; break;
                case 'b':
                case 'B': c_ch = 11; break;
                case 'c':
                case 'C': c_ch = 12; break;
                case 'd':
                case 'D': c_ch = 13; break;
                case 'e':
                case 'E': c_ch = 14; break;
                case 'f':
                case 'F': c_ch = 15; break;
            }

            c_buf[c_que] = c_ch;
            c_que++;
            if ( c_que > 4 )
                return;
            src++;
        }

        *out = 0;


        for(cnt=0;cnt<c_que-1;cnt++)
        {
            *out += c_buf[cnt] * upow( 16 , c_que - cnt - 1 );
        }
        *out += c_buf[cnt];

    }
}

bool convert_macstr_to_macarray( const char* ms, unsigned char* ma )
{
    if ( ( ms == NULL ) || ( ma == NULL ) )
    {
        return false;
    }

    char* srcs = strdup( ms );
    char* stok = strtok( srcs, ":" );
    int   ncnt = 0;

    while( stok != NULL )
    {
        //uatoh( s, &tox);
        //unsigned in = atoi( stok );
        //ma[ncnt] = in;
        ncnt++;
        if ( ncnt > 5 )
            break;
        stok = strtok( NULL, ":" );
    }

    free( srcs );

    return true;
}

void releaseWorkParm( D_WORK_PARAM* dwp )
{
    if ( dwp != NULL )
    {
        if ( dwp->param1 != NULL )
        {
            free( dwp->param1 );
        }

        if ( dwp->param2 != NULL )
        {
            free( dwp->param2 );
        }

        if ( dwp->param3 != NULL )
        {
            free( dwp->param3 );
        }

        delete dwp;
        dwp = NULL;
    }
}

void killLedOnProc()
{
    pid_t pid = -1;
    char  cmdstr[64] = {0};

    pid = getPIDbyname( proc_led_app, false, NULL );

    if ( pid > 0 )
    {
        sprintf( cmdstr, "kill -9 %d", pid );
        system( cmdstr );

        // Wait for process killed.
        while ( pid > 0 )
        {
            safe_usleep( 100 * 1000 ); /// just wait for 100ms.
            pid = getPIDbyname( proc_led_app, false, NULL );
        }
    }

    // Wait for 1 second.
    sleep( 1 );
}

void killSysProcs()
{
    pid_t pid = -1;
    char  cmdstr[64] = {0};

    for( int cnt=0; cnt<proclists; cnt++ )
    {
        pid = getPIDbyname( proclist[cnt], false, NULL );

        if ( pid > 0 )
        {
            sprintf( cmdstr, "kill -9 %d", pid );
            system( cmdstr );

            // Wait for process killed.
            while ( pid > 0 )
            {
                safe_usleep( 100 * 1000 ); /// just wait for 100ms.
                pid = getPIDbyname( proclist[cnt], false, NULL );
            }
        }
    }
}

void killWifiProcs()
{
    pid_t pid = -1;
    char  cmdstr[64] = {0};

    const char* killlist[] = { proc_wifi_cli, proc_wifi_dhcpc, proc_wifi_dhcpd, proc_wifi_ap };

    for( int cnt=0; cnt<4; cnt++ )
    {
#ifdef DEBUG
        printf("### killing process : %s ... ", killlist[cnt] );
#endif // DEBUG
        pid = getPIDbyname( killlist[cnt], false, NULL );

        if ( pid > 0 )
        {
            sprintf( cmdstr, "kill -9 %d", pid );
            system( cmdstr );

            // Wait for process killed.
            while ( pid > 0 )
            {
                safe_usleep( 100 * 1000 ); /// just wait for 100ms.
                pid = getPIDbyname( killlist[cnt], false, NULL );
            }
        }
#ifdef DEBUG
        printf("done.\n");
#endif // DEBUG
    }
}

void killSFTPD()
{
    pid_t pid = -1;
    char  cmdstr[64] = {0};

#ifdef DEBUG
    printf("### killing process : %s ... ", DEF_AS_SFTPD_BIN );
#endif // DEBUG
    pid = getPIDbyname( DEF_AS_SFTPD_BIN, false, NULL );

    if ( pid > 0 )
    {
        sprintf( cmdstr, "kill -9 %d", pid );
        system( cmdstr );

        // Wait for process killed.
        while ( pid > 0 )
        {
            safe_usleep( 100 * 1000 ); /// just wait for 100ms.
            pid = getPIDbyname( DEF_AS_SFTPD_BIN, false, NULL );
        }
    }
#ifdef DEBUG
    printf("done.\n");
#endif // DEBUG
}

bool check_cmd_exists( const char* buff, const char* demand )
{
    if ( buff == NULL )
        return false;

    if ( demand == NULL )
        return false;

    int szdemand = strlen( demand );

    if ( strncmp(buff, demand, szdemand ) == 0 )
        return true;

    return false;
}

int proc_usercontrol( const char* cmd, int sockid )
{
#ifdef DEBUG
    printf("Processing : %s\n", cmd );
#endif // DEBUG

    static const char strnotsupported[] = "NOT_SUPPORT";

    if ( check_cmd_exists( cmd, "UC+GETMACADDR=" ) == true )
    {
        const char* popt = &cmd[14];

        D_WORK_PARAM* newworkparam = new D_WORK_PARAM();

        if ( newworkparam != NULL )
        {
            newworkparam->wtype = D_WORK_NONE;
            newworkparam->clisockfd = sockid;
            newworkparam->param1 = NULL;
            newworkparam->param2 = NULL;
            newworkparam->param3 = NULL;

            if ( strcmp( popt, "ETH" ) == 0 )
            {
                newworkparam->wtype = D_WORK_GETMACADDR_ETH;
            }
            else
            if ( strcmp( popt, "WLAN" ) == 0 )
            {
                if ( opt_wifi == false )
                {
                    releaseWorkParm( newworkparam );

                    //send2clients( strnotsupported, strlen( strnotsupported ) );
                    send( sockid , strnotsupported, strlen( strnotsupported ), MSG_NOSIGNAL );

                    return 0;
                }
                else
                {
                    newworkparam->wtype = D_WORK_GETMACADDR_WLAN;
                }
            }

            if ( newworkparam->wtype != D_WORK_NONE )
            {
                pthread_create( &pt_id_oncework,
                                NULL,
                                pthread_once_proc,
                                newworkparam );
            }
            else
            {
                releaseWorkParm( newworkparam );
            }
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+GETIPADDR=" ) == true )
    {
        const char* popt = &cmd[13];

        D_WORK_PARAM* newworkparam = new D_WORK_PARAM();

        if ( newworkparam != NULL )
        {
            newworkparam->wtype = D_WORK_NONE;
            newworkparam->clisockfd = sockid;
            newworkparam->param1 = NULL;
            newworkparam->param2 = NULL;
            newworkparam->param3 = NULL;

            if ( strcmp( popt, "ETH" ) == 0 )
            {
                newworkparam->wtype = D_WORK_GETIPADDR_ETH;
            }
            else
            if ( strcmp( popt, "WLAN" ) == 0 )
            {
                if ( opt_wifi == false )
                {
                    releaseWorkParm( newworkparam );

                    //send2clients( strnotsupported, strlen( strnotsupported ) );
                    send( sockid, strnotsupported, strlen( strnotsupported ), MSG_NOSIGNAL );

                    return 0;
                }
                else
                {
                    newworkparam->wtype = D_WORK_GETIPADDR_WLAN;
                }
            }

            if ( newworkparam->wtype != D_WORK_NONE )
            {
                pthread_create( &pt_id_oncework,
                                NULL,
                                pthread_once_proc,
                                newworkparam );
            }
            else
            {
                releaseWorkParm( newworkparam );
            }
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+SETIPADDR=" ) == true )
    {
        const char* popt = &cmd[13];

        D_WORK_PARAM* newworkparam = new D_WORK_PARAM();

        if ( newworkparam != NULL )
        {
            char* dev = NULL;

            newworkparam->wtype = D_WORK_NONE;
            newworkparam->clisockfd = sockid;
            newworkparam->param1 = NULL;
            newworkparam->param2 = NULL;
            newworkparam->param3 = NULL;

            getoptparams3( popt,
                           (char**)&dev,
                           (char**)&newworkparam->param1,
                           (char**)&newworkparam->param2 );

            if ( dev != NULL )
            {
                if ( strcmp( dev, "ETH" ) == 0 )
                {
                    newworkparam->wtype = D_WORK_SETIPADDR_ETH;
                }
                else
                if ( strcmp( dev, "WLAN" ) == 0 )
                {
                    if ( opt_wifi == false )
                    {
                        releaseWorkParm( newworkparam );

                        //send2clients( strnotsupported, strlen( strnotsupported ) );
                        send( sockid, strnotsupported, strlen( strnotsupported ), MSG_NOSIGNAL );

                        return 0;
                    }
                    else
                    {
                        newworkparam->wtype = D_WORK_SETIPADDR_WLAN;
                    }
                }
            }

            if ( newworkparam->wtype != D_WORK_NONE )
            {
                pthread_create( &pt_id_oncework,
                                NULL,
                                pthread_once_proc,
                                newworkparam );
            }
            else
            {
                if ( dev != NULL )
                {
                    free( dev );
                }

                releaseWorkParm( newworkparam );
            }
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+SETWIFI=" ) == true )
    {
        if ( opt_wifi == false )
        {
            //send2clients( strnotsupported, strlen( strnotsupported ) );
            send( sockid, strnotsupported, strlen( strnotsupported ), MSG_NOSIGNAL );
            return 0;
        }

        const char* popt = &cmd[11];

        D_WORK_PARAM* newworkparam = new D_WORK_PARAM();

        if ( newworkparam != NULL )
        {
            char* wmode = NULL;

            newworkparam->wtype = D_WORK_NONE;
            newworkparam->clisockfd = sockid;
            newworkparam->param1 = NULL;
            newworkparam->param2 = NULL;
            newworkparam->param3 = NULL;

            getoptparams4( popt,
                           (char**)&wmode,
                           (char**)&newworkparam->param1,
                           (char**)&newworkparam->param2,
                           (char**)&newworkparam->param3 );

            if ( wmode != NULL )
            {
                if ( strcmp( wmode, "OFF" ) == 0 )
                {
                    newworkparam->wtype = D_WORK_SETWIFI_OFF;
                }
                else
                if ( strcmp( wmode, "AP" ) == 0 )
                {
                    newworkparam->wtype = D_WORK_SETWIFI_AP;
                }
                else
                if ( strcmp( wmode, "CLI" ) == 0 )
                {
                    newworkparam->wtype = D_WORK_SETWIFI_CLI;
                }
            }

            if ( newworkparam->wtype != D_WORK_NONE )
            {
                pthread_create( &pt_id_oncework,
                                NULL,
                                pthread_once_proc,
                                newworkparam );
            }
            else
            {
                if ( wmode != NULL )
                {
                    free( wmode );
                }

                releaseWorkParm( newworkparam );
            }

        }
    }

    if ( check_cmd_exists( cmd, "UC+GETSTATE" ) == true )
    {
        unsigned int* workid = new unsigned int;

        if ( workid != NULL )
        {
            *workid = D_WORK_GETSTATE;

            pthread_create( &pt_id_oncework,
                            NULL,
                            pthread_once_proc,
                            workid );
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+GETVERSION" ) == true )
    {
        //send2clients( version_full_str, strlen( version_full_str ) );
        send( sockid, version_full_str, strlen( version_full_str ), MSG_NOSIGNAL );

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+GETLOG" ) == true )
    {
        if ( ( logBuff != NULL ) && ( logSz > 0 ) )
        {
            //write( sockid, logBuff, logSz );
            send( sockid, logBuff, logSz, MSG_NOSIGNAL );
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+FACTORYRESET" ) == true )
    {
        unsigned int* workid = new unsigned int;

        if ( workid != NULL )
        {
            *workid = D_WORK_FACTORY_RESET;

            pthread_create( &pt_id_oncework,
                            NULL,
                            pthread_once_proc,
                            workid );
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+FWUPGRADETOGGLE" ) == true )
    {
        //D_WORK_FW_UPGRADE
        unsigned int* workid = new unsigned int;

        if ( workid != NULL )
        {
            *workid = D_WORK_FW_UPGRADE_TOGGLE;

            pthread_create( &pt_id_oncework,
                            NULL,
                            pthread_once_proc,
                            workid );
        }

        return 0;
    }

    if ( check_cmd_exists( cmd, "UC+FWUPGRADESTART" ) == true )
    {
        //D_WORK_FW_UPGRADE
        unsigned int* workid = new unsigned int;

        if ( workid != NULL )
        {
            *workid = D_WORK_FW_UPGRADE_START;

            pthread_create( &pt_id_oncework,
                            NULL,
                            pthread_once_proc,
                            workid );
        }

        return 0;
    }


    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int main_loop()
{
    pt_nm = pthread_create( &pt_id_read, NULL, pthread_loop_udp, NULL );
    if ( pt_nm < 0 )
    {
        perror("pthread error in creation (pthread_loop_udp) .");

        pt_lived = false;

        return -1;
    }

    pt_nm = pthread_create( &pt_id_tcp, NULL, pthread_loop_socket_tcp, NULL );
    if ( pt_nm < 0 )
    {
        perror("pthread error in creation (pthread_loop_socket_tcp) .");

        pt_lived = false;
        pthread_kill( pt_id_read, NULL );

        return -1;
    }

    pt_nm = pthread_create( &pt_id_netwatch, NULL, pthread_loop_netwatch, NULL );
    if ( pt_nm < 0 )
    {
        perror("pthread error in creation (pthread_loop_netwatch) .");

        pt_lived = false;
        pthread_kill( pt_id_read, NULL );
        pthread_kill( pt_id_tcp, NULL );

        return -1;
    }


    while( pt_lived == true )
    {
        sleep(1);
    }

    flag_quit = true;

    pthread_kill( pt_id_tcp, NULL );

    close_sock_udp();
    close_sock_tcp();

    return 0;
}

////////////////////////////////////////////////////////////////////////////////

void* pthread_loop_udp( void* p )
{
    while( pt_lived == true )
    {
        int reti = -1;

        memset( udp_recvbuff, 0, DEF_SZ_READ );

        /*
        reti = recvfrom( sockfd_udp_cli,
                         (void*)udp_recvbuff,
                         DEF_SZ_READ ,
                         0,
                         NULL,
                         NULL );
        */
        reti = recv( sockfd_udp_cli, (void*)udp_recvbuff, DEF_SZ_READ , 0 );

        if ( reti > 0 )
        {
            if ( clients_cnt > 0 )
            {
                // Just send it as string.
                int recvbuff_sz = strlen( udp_recvbuff );

                if ( recvbuff_sz > 0 )
                {
                    send2clients( udp_recvbuff, recvbuff_sz );
                }
            }
        }

        safe_usleep( 1000 );  /// let sleep in 1ms.
    }
}

void* pthread_loop_socket_tcp( void* p )
{
    add_log("entering TCP thread ...");

    client_fdsock = server_fdsock;

    int maxi = -1;

    int maxfd = server_fdsock;

    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
    {
        clients[cnt] = -1;
    }

    FD_ZERO( &readfds );
    FD_SET( server_fdsock, &readfds );

    add_log("FD process completed.");

    bool firstrun = true;

    while( flag_quit == false )
    {
        if ( firstrun == true )
        {
            add_log("Proceeding sock selection first ...");
        }

        int cnt = 0;
        allfds  = readfds;
        fd_num  = select(maxfd + 1 , &allfds, (fd_set *)0, (fd_set *)0, NULL);

        if ( firstrun == true )
        {
            firstrun = false;

            add_log("Completed sock selection");
        }

        if ( send2clients_inuse == true )
            continue;

        if ( FD_ISSET( server_fdsock, &allfds ) )
        {
            client_len = sizeof(clientaddr);
            client_fdsock = accept(server_fdsock, (struct sockaddr *)&clientaddr, &client_len);

            for (cnt=0; cnt<FD_SETSIZE; cnt++)
            {
                if ( clients[cnt] < 0 )
                {
                    clients[cnt] = client_fdsock;
                    break;
                }
            }

            if ( cnt == FD_SETSIZE )
            {
                close( fdsock );
                clients[cnt] = -1;
            }

            FD_SET(client_fdsock,&readfds);

            if ( client_fdsock > maxfd )
                maxfd = client_fdsock;

            if ( cnt > maxi )
            {
                maxi = cnt;
                clients_max = maxi;
            }

            clients_cnt++;

            if ( --fd_num <= 0 )
            {
                safe_usleep( 1000*100 ); /// maybe 100ms waits.
                continue;
            }
        }

        for (cnt=0; cnt<=maxi; cnt++)
        {
            if ( ( fdsock = clients[ cnt ] ) < 0 )
            {
                safe_usleep( 1000*100 ); /// maybe 100ms waits.
                continue;
            }

            if ( FD_ISSET( fdsock, &allfds ) )
            {
                memset( sock_readbuff, 0 , DEF_SZ_READ );

                int szread = read_tcp_nb_client( fdsock, &readfds );

                if ( szread < 0 )
                {
                    close(fdsock);
                    FD_CLR(fdsock, &readfds);
                    clients[cnt] = -1;

                    if ( clients_cnt > 0 )
                    {
                        clients_cnt--;
                    }
                }
                else
                {
                    // --------------------------------------------------------
                    // Handle received command here :
                    // --------------------------------------------------------
                    if ( strncmp( sock_readbuff, "UC+", 3 ) == 0 )
                    {
                        if ( check_cmd_exists( sock_readbuff, "UC+CLOSE" ) == true )
                        {
                            char outstr[] = "CLOSE_CONNECTION\n";
                            send( fdsock, outstr, strlen( outstr ), MSG_NOSIGNAL );
                            safe_usleep( 300 * 1000 );
                            close( fdsock );

                            if ( clients_cnt > 0 )
                            {
                                clients_cnt--;
                            }

                            FD_CLR( fdsock, &readfds );
                            clients[cnt] = -1;
                            break;
                        }
                        else
                        if ( check_cmd_exists( sock_readbuff, "UC+REBOOT" ) == true )
                        {
                            // block recv .
                            rcv_blocked = true;

                            char outstr[64] = {0};

                            // kill all processes.
                            killSysProcs();
                            system("sync");

                            strcpy( outstr, "SYSTEM_REBOOT\n" );
                            send2clients( outstr, strlen( outstr ) );
                            safe_usleep( 300 * 1000 );

                            strcpy( outstr, "CLOSE_CONNECTION\n" );
                            send2clients( outstr, strlen( outstr ) );
                            safe_usleep( 300 * 1000 );

                            close( fdsock );
                            FD_CLR( fdsock, &readfds );
                            clients[cnt] = -1;

                            // reboot system.
                            sys_need_reboot = true;
                            safe_usleep( 1000 * 1000 );
                            system("reboot -f");
                            exit( 0 );
                            break;
                        }
                        else
                        {
                            if ( rcv_blocked == true )
                            {
                                send( fdsock, "SERVER_BUSY\n", 12, MSG_NOSIGNAL );
                                break;
                            }

                            proc_usercontrol( sock_readbuff, fdsock );
                        }
                    }
                    // --------------------------------------------------------
                    else
                    if ( strncmp( sock_readbuff, "AT+", 3 ) == 0 )
                    {
                        int reti = sendto( sockfd_udp_svr,
                                           sock_readbuff,
                                           szread,
                                           0,
                                           (struct sockaddr *)&server_udp_addr,
                                           sock_udp_clilen );
                        safe_usleep( 1000 );
                    }
                    else
                    {
                        memset( sock_readbuff, 0 , DEF_SZ_READ );
                    }
                }

                if ( --fd_num <= 0 )
                    break;
            }
            else
            {
                safe_usleep( 1000 * 500 ); /// maybe 0.5 sec waits.
            }
        }

        safe_usleep( 1000 * 100 ); /// maybe 100ms waits.
    }

    return NULL;
}

void* pthread_loop_netwatch( void* p )
{
#ifdef DEBUG
    printf("*** entering pthread_loop_netwatch() ***\n");
#endif // DEBUG
    // Just wait for a second.
    safe_usleep( 1000 * 1000 );

    while( flag_quit == false )
    {
        // Check IP allocated ...
        int tmp_sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_IP );

        if ( tmp_sock >= 0 )
        {
            struct  ifreq   ifr;

            strcpy( ifr.ifr_name, "eth0" );

            if ( ioctl( tmp_sock, SIOCGIFADDR, &ifr ) != 0 )
            {
                pid_t pid = getPIDbyname( proc_wifi_dhcpc, false, NULL );
                if ( pid > 0 )
                {
                    char cmdstr[256] = {0};

                    sprintf( cmdstr, "kill -9 %d", pid );
                    system( cmdstr );

                    // Wait for process killed.
                    while ( pid > 0 )
                    {
                        safe_usleep( 100000 ); /// just wait for 100ms.
                        pid = getPIDbyname( proc_wifi_dhcpc, false, NULL );
                    }
                }

                system("udhcpc -R -n -p /var/run/udhcpc.eth0.pid -i eth0");
            }
        }

        // Wait for a 5 sec.
        safe_usleep( 1000 * 1000 * 5 );
    }

#ifdef DEBUG
    printf("*** finishing pthread_loop_netwatch() ***\n");
#endif // DEBUG

    return NULL;
}

// This pthread call function executes heavy works as asynchronous response.
void* pthread_once_proc( void* p )
{
    D_WORK_PARAM* work_parm = (D_WORK_PARAM*)p;

    if ( work_parm == NULL )
    {
        return NULL;
    }

    ctrl_set_led( false, true, false );

    switch( work_parm->wtype )
    {
        case D_WORK_GETMACADDR_ETH:
        case D_WORK_GETMACADDR_WLAN:
            {
                // Code from :
                // http://stackoverflow.com/questions/1779715/how-to-get-mac-address-of-your-machine-using-a-c-program
                struct  ifreq   ifr;
                char    ifdata[1024] = {0};
                int     tmp_sock = -1;

                tmp_sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_IP );
                if ( tmp_sock >= 0 )
                {
                    if ( work_parm->wtype == D_WORK_GETMACADDR_WLAN )
                    {
                        strcpy( ifr.ifr_name, "wlan0" );
                    }
                    else
                    {
                        strcpy( ifr.ifr_name, "eth0" );
                    }

                    if ( ioctl( tmp_sock, SIOCGIFHWADDR, &ifr )  == 0 )
                    {
                        char macstr[64] = {0};

                        sprintf( macstr,
                                 "%02X:%02X:%02X:%02X:%02X:%02X\n",
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[0],
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[1],
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[2],
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[3],
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[4],
                                 (unsigned char)ifr.ifr_hwaddr.sa_data[5] );

                        bool retb = false;
                        while( retb == false )
                        {
                            retb = send2client( work_parm->clisockfd,
                                                macstr,
                                                strlen( macstr ) );
                        }

                        close( tmp_sock );
                    }
                }

            }
            break;

        case D_WORK_GETIPADDR_ETH:
        case D_WORK_GETIPADDR_WLAN:
            {
                struct  ifreq   ifr;
                char    ifdata[1024] = {0};
                int     tmp_sock = -1;

                tmp_sock = socket( AF_INET, SOCK_DGRAM, IPPROTO_IP );
                if ( tmp_sock >= 0 )
                {
                    if ( work_parm->wtype == D_WORK_GETIPADDR_WLAN )
                    {
                        strcpy( ifr.ifr_name, "wlan0" );
                    }
                    else
                    {
                        strcpy( ifr.ifr_name, "eth0" );
                    }

                    char ipstr[64] = {0};

                    if ( ioctl( tmp_sock, SIOCGIFADDR, &ifr )  == 0 )
                    {
                        sprintf( ipstr,
                                 "%s\n",
                                 inet_ntoa( ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr ) );

                        bool retb = false;
                        while( retb == false )
                        {
                            retb = send2client( work_parm->clisockfd,
                                                ipstr,
                                                strlen( ipstr ) );
                        }
                    }
                    else
                    {
                        sprintf( ipstr, "OFFLINE\n");

                        bool retb = false;
                        while( retb == false )
                        {
                            retb = send2client( work_parm->clisockfd,
                                                ipstr,
                                                strlen( ipstr ) );
                        }

                    }
                }
            }
            break;

        case D_WORK_GETSTATE: /// returns alive state of daemons.
            {
                rcv_blocked = true;

                char statestr[256] = {0};

                for( int cnt=0; cnt<proclists; cnt++ )
                {
                    char tmpstr[64] = {0};

                    pid_t tmppid = getPIDbyname( proclist[cnt], false, NULL );

                    sprintf( tmpstr, "%s:%d,", proclist[cnt], tmppid );
                    strcat( statestr, tmpstr );
                }

                bool retb = false;
                while( retb == false )
                {
                    retb = send2client( work_parm->clisockfd,
                                        statestr,
                                        strlen( statestr ) );
                }

                rcv_blocked = false;
            }
            break;

        case D_WORK_SETIPADDR_ETH:
        case D_WORK_SETIPADDR_WLAN:
            {
                char tmpstr[256] = "NOT_ALLOWED\n";

                bool retb = false;
                while( retb == false )
                {
                    retb = send2client( work_parm->clisockfd,
                                        tmpstr,
                                        strlen( tmpstr ) );
                }
            }
            break;

        case D_WORK_SETWIFI_OFF:
            {
                rcv_blocked = true;

                char respstr[32] = {0};

                killWifiProcs();

                sprintf( respstr, "WIFI_CHANGING\n" );

                send2clients( respstr, strlen( respstr ) );

                system("ifconfig wlan0 down");

                sprintf( respstr, "WIFI_OFF\n" );
                send2clients( respstr, strlen( respstr ) );

                rcv_blocked = false;
            }
            break;

        case D_WORK_SETWIFI_AP:
            {
                rcv_blocked = true;

                char respstr[32] = {0};

                // check current WIFI procs..
                pid_t idd = getPIDbyname( proc_wifi_ap, false, NULL );
                if ( idd > 0 )
                {
                    // already AP mode.
                    sprintf( respstr, "ALREADY_WIFI_AP\n" );
                    send2client( work_parm->clisockfd, respstr, strlen( respstr ) );

                    rcv_blocked = false;
                    break;
                }

                sprintf( respstr, "WIFI_CHANGING\n" );
                send2clients( respstr, strlen( respstr ) );

                killWifiProcs();

                system("ifconfig wlan0 down");
                system("ifconfig wlan0 192.168.42.1");
                system("udhcpd /etc/udhcpd.conf");
                system("hostapd /etc/hostapd.conf &");
                sleep(2);

                idd = getPIDbyname( proc_wifi_ap, false, NULL );
                if ( idd > 0 )
                {
                    sprintf( respstr, "WIFI_READY\n" );
                    send2clients( respstr, strlen( respstr ) );
                }
                else
                {
                    // If failure, need kill process ...
                    system("ifconfig wlan0 down");

                    killWifiProcs();

                    sprintf( respstr, "WIFI_AP_FAILURE\n" );
                    send2clients( respstr, strlen( respstr ) );
                }

                rcv_blocked = false;
            }
            break;

        case D_WORK_SETWIFI_CLI:
            {
                if ( work_parm->param1 == NULL )
                    break;

                rcv_blocked = true;

                char respstr[80] = {0};

                // check current WIFI procs..
                pid_t idd = getPIDbyname( proc_wifi_cli, false, NULL );
                if ( idd > 0 )
                {
                    // already AP mode.
                    sprintf( respstr, "ALREADY_WIFI_CLI\n" );
                    send2client( work_parm->clisockfd, respstr, strlen( respstr ) );

                    rcv_blocked = false;
                    break;
                }

                sprintf( respstr, "WIFI_CHANGING\n" );
                send2clients( respstr, strlen( respstr ) );

                killWifiProcs();

                int reti = load_wifi_info();
                if ( reti <= 0 )
                {
                    sprintf( respstr, "WIFI_FAILURE:NO_WPA_SUPPLICANT\n" );
                    send2clients( respstr, strlen( respstr ) );

                    rcv_blocked = false;
                    break;
                }

                int new_w_type = WIFI_T_NONE;
                char ssid[ MAX_WIFI_SSID_LEN ] = {0};
                char psk[ MAX_WIFI_PSK_LEN ] = {0};

                if ( strcmp( work_parm->param1, "OPEN" ) == 0 )
                {
                    new_w_type = WIFI_T_OPEN;
                }
                else
                if ( strcmp( work_parm->param1, "WEP" ) == 0 )
                {
                    new_w_type = WIFI_T_WEP;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }
                else
                if ( strcmp( work_parm->param1, "WPATKIP" ) == 0 )
                {
                    new_w_type = WIFI_T_WEP;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }
                else
                if ( strcmp( work_parm->param1, "WPAAES" ) == 0 )
                {
                    new_w_type = WIFI_T_WPA2AES;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }
                else
                if ( strcmp( work_parm->param1, "WPA2TKIP" ) == 0 )
                {
                    new_w_type = WIFI_T_WPA2TKIP;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }
                else
                if ( strcmp( work_parm->param1, "WPA2AES" ) == 0 )
                {
                    new_w_type = WIFI_T_WPA2AES;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }
                else
                if ( strcmp( work_parm->param1, "WPA2BOTH" ) == 0 )
                {
                    new_w_type = WIFI_T_WPA2BOTH;
                    strncpy( ssid, work_parm->param2, MAX_WIFI_SSID_LEN );
                    strncpy( psk, work_parm->param3, MAX_WIFI_PSK_LEN );
                }

                if ( new_w_type > WIFI_T_NONE )
                {
                    int retwifii = 0;

                    if ( new_w_type == WIFI_T_OPEN )
                    {
                        retwifii = change_wifi_info( new_w_type, NULL, NULL );
                    }
                    else
                    if ( new_w_type >= WIFI_T_OPEN )
                    {
                        if ( ( strlen( psk ) > 0 ) && ( strlen( ssid ) > 0 ) )
                        {
                            retwifii = change_wifi_info( new_w_type, ssid, psk );
                        }
                        else
                        {
                            sprintf( respstr, "WIFI_FAILURE:PARAM_ERROR\n" );
                            send2clients( respstr, strlen( respstr ) );

                            rcv_blocked = false;
                            break;
                        }
                    }

                    if ( retwifii == 0 )
                    {
                        save_wifi_info();

                        system("ifconfig wlan0 down");
                        system("wpa_supplicant -B -Dnl80211 -i wlan0 -c /etc/wpa_supplicant.conf");
                        sleep(1);
                        system("udhcpc -i wlan0 &");
                        sleep(1);

                        sprintf( respstr, "WIFI_CLI_APPLIED\n" );
                        send2clients( respstr, strlen( respstr ) );
                    }
                    else
                    {
                        sprintf( respstr, "WIFI_CLI_FAILED\n" );
                        send2clients( respstr, strlen( respstr ) );
                    }
                }
                else
                {
                    sprintf( respstr, "WIFI_FAILURE:NOTHING\n" );
                    send2clients( respstr, strlen( respstr ) );
                }

                rcv_blocked = false;
            }
            break;

        case D_WORK_FACTORY_RESET:
            {
                rcv_blocked = true;

                char respstr[80] = {0};

                sprintf( respstr, "ENTER:FACTORYRESET\n" );
                send2clients( respstr, strlen( respstr ) );

                // Check firmware exists ...
                if ( access( ETC_TARGZ_F, F_OK ) == 0 )
                {
                    sprintf( respstr, "PROGRESS:FACTORYRESET\n" );
                    send2clients( respstr, strlen( respstr ) );

                    system( "rm -rf /etc/*" );

                    //tar -zxf /usr/local/firmware/default_flashfs_files.tar.gz -C /
                    char cmdstr[256] = {0};

                    sprintf( cmdstr,
                             "tar -zxf %s -C /",
                             ETC_TARGZ_F );

                    system( cmdstr );
                    system( "sync" );

                    // Wait for system in ready.
                    sleep( 1 );

                    sprintf( respstr, "DONE:FACTORYRESET\n" );
                    send2clients( respstr, strlen( respstr ) );

                    sprintf( respstr, "SYSTEM_REBOOT\n" );
                    send2clients( respstr, strlen( respstr ) );

                    sprintf( respstr, "CLOSE_CONNECTION\n" );
                    send2clients( respstr, strlen( respstr ) );

                    close_sock_tcp();
                    close_sock_udp();

                    sleep( 1 );
                    sys_need_reboot = true;
                    system( "reboot -f" );
                    exit( 0 );
                    return NULL;
                }
                else
                {
                    sprintf( respstr, "FAILURE:FW_NOT_FOUND_OR_ACCESS_DENIDED\n" );
                    send2clients( respstr, strlen( respstr ) );

                    break;
                }
            }
            break;

        case D_WORK_FW_UPGRADE_TOGGLE:
            {
                rcv_blocked = true;

                char tmpstr[80] = {0};

                killSFTPD();

                // Let start as_sftpd.
                sprintf( tmpstr, "%s %s", DEF_AS_SFTPD_BIN, DEF_AS_SFTPD_PARAM );
                system( tmpstr );

                // global msg to all.
                sprintf( tmpstr, "ENTER:FWUPGRADESTATE\n" );
                send2clients( tmpstr, strlen( tmpstr ) );

                // send feedback to sender.
                sprintf( tmpstr, "START_FW_UPLOAD" );

                bool retb = false;
                while( retb == false )
                {
                    retb = send2client( work_parm->clisockfd,
                                        tmpstr,
                                        strlen( tmpstr ) );
                }

                rcv_blocked = false;
            }
            break;

        case D_WORK_FW_UPGRADE_START:
            {
                rcv_blocked = true;

                // Let stop FTPd.
                killSFTPD();

                char tmpstr[80] = {0};
                bool retb = false;

                // check fw file exists.
                if ( access( DEF_AS_FW_FILE, F_OK ) != 0 )
                {
                    sprintf( tmpstr, "ERROR:NO_FW_FILE" );
                    while( retb == false )
                    {
                        retb = send2client( work_parm->clisockfd,
                                            tmpstr,
                                            strlen( tmpstr ) );
                    }

                    rcv_blocked = false;
                    break;
                }
                else
                {
                    sprintf( tmpstr, "FW_UPGRADE_IN_PROGRESS" );
                    while( retb == false )
                    {
                        retb = send2client( work_parm->clisockfd,
                                            tmpstr,
                                            strlen( tmpstr ) );
                    }

                    // Let run as_fw_writer.
                    system( DEF_AS_FW_WRITER );

                    // Check result file .
                    if ( access( DEF_AS_FW_RESULT, F_OK ) != 0 )
                    {
                        sprintf( tmpstr, "FW_UPGRADE_FAILURE:NORESULT" );
                        while( retb == false )
                        {
                            retb = send2client( work_parm->clisockfd,
                                                tmpstr,
                                                strlen( tmpstr ) );
                        }
                        break;
                    }
                    else
                    {
                        FILE* fp = fopen( DEF_AS_FW_RESULT , "r" );
                        if ( fp == NULL )
                        {
                            sprintf( tmpstr, "FW_UPGRADE_FAILURE:RESULTFAILURE-0" );
                            while( retb == false )
                            {
                                retb = send2client( work_parm->clisockfd,
                                                    tmpstr,
                                                    strlen( tmpstr ) );
                            }
                            break;
                        }
                        else
                        {
                            char teststr[40] = {0};

                            fscanf( fp, "%s", teststr );
                            fclose( fp );

                            if ( strncmp( teststr, "COMPLETE", 8 ) != 0 )
                            {
                                sprintf( tmpstr, "FW_UPGRADE_FAILURE:RESULTFAILURE-1" );
                                while( retb == false )
                                {
                                    retb = send2client( work_parm->clisockfd,
                                                        tmpstr,
                                                        strlen( tmpstr ) );
                                }
                                break;
                            }
                            else
                            {
                                sprintf( tmpstr, "FW_UPGRADE_COMPLETED" );
                                while( retb == false )
                                {
                                    retb = send2client( work_parm->clisockfd,
                                                        tmpstr,
                                                        strlen( tmpstr ) );
                                }

                                // reboot me after 1 second.
                                sleep( 1 );

                                system( "reboot -f" );
                            }
                        }
                    }
                }

                // rcv_blocked = false; <= no more recv allowed until reboot.
            }
            break;
    }

    // release work params ...
    releaseWorkParm( work_parm );

    ctrl_set_led( false, false, true );

    return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool sigint_mutexed = false;

void sigint_handler( int signo )
{
    printf("sigint_handler()!!!\n");
    //signal( SIGINT, NULL );
    if ( sigint_mutexed == false )
    {
        sigint_mutexed = true;
        printf("sending thread kill signal...");

        pt_lived = false;
        flag_quit = true;

        pthread_kill( pt_id_tcp, NULL );
        pthread_kill( pt_id_read, NULL );

        //pthread_join( pt_id_read, NULL );

        close_led_devs();
        close_sock_udp();
        close_sock_tcp();

        //release log
        release_log();

        if ( sys_need_reboot == true )
        {
            system("reboot -f");
        }
#ifdef AS_DAEMON
        else
        {
            // re-run myself.
            system("asctrld restart");
        }
#endif // AS_DAEMON

        exit( 0 );
    }
}

int main (int argc, char ** argv)
{
    load_all_versions();

    // Create log memory ...
    alloc_log();

    printf("Anystream UDP/lo control server, version=%s\n", VER_STR);
    printf("(C)2015 3Iware, Raph.K / rage.kim@3iware.com\n\n");

#ifdef DEBUG
    printf("*DEBUG*\n\n");
#endif // DEBUG

    signal( SIGINT, sigint_handler );

    killLedOnProc();

    if ( open_led_devs() == false )
    {
        add_log("LED config failure !");
    }

    // set LED : RED
    ctrl_set_led( true, false, false );

    if ( open_sock_udp() == false )
    {
        add_log("open UDP failure !");
        release_log();
        return -1;
    }
    else
    {
        add_log("open UDP succeed.");
    }

    if ( open_sock_tcp() == false )
    {
        add_log("open TCP failure !");
        release_log();
        return -1;
    }
    else
    {
        add_log("open TCP succeed.");
    }

    printf("\n");

    int reti = 0;

#ifdef WIFI_ENABLED
    opt_wifi = true;
#endif // WIFI_ENABLED

    // set LED : BLUE
    ctrl_set_led( false, false, true );

#ifdef AS_DAEMON
    if ( fork() == 0 )
    {
        add_log("DAEMON mode enabled.");
        reti = main_loop();
    }
    else
    {
        exit( 0 );
    }
#else
    reti = main_loop();
#endif /// of AS_DAEMON

    add_log("main_loop() ended\n");

    close_led_devs();
    release_log();

    if ( sys_need_reboot == true )
    {
        system("reboot -f");
    }
#ifdef AS_DAEMON
    else
    {
        // re-run myself.
        system("asctrld restart");
    }
#endif // AS_DAEMON
    return reti;
}
