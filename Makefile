# Makefile for asctrld,
# scripted by Raph.K. (rage.kim@3iware.com)

# Notice :
# You may need check 'env' to exists TOOLSDIR.

#CCPATH = /opt/Mozart_toolchain/arm-eabi-uclibc/usr
#CCBIN  = ${CCPATH}/bin
CCBIN    = ${TOOLSDIR}
CCPREFIX = arm-linux-

GCC = ${CCBIN}/${CCPREFIX}gcc
GPP = ${CCBIN}/${CCPREFIX}g++
AR  = ${CCBIN}/${CCPREFIX}ar

SOURCEDIR  = ./src
OBJDIR     = ./obj/Release
OUTBINNAME = asctrld
OUTPUTDIR = ./bin/Release
DEFINEOPT = -D_GNU_SOURCE
OPTIMIZEOPT = -O2 -fexpensive-optimizations

CFLAGS := -I$(SOURCEDIR) -D$(DEFINEOPT) $(OPTMIZEOPT)

all: ${OUTPUTDIR}/${OUTBINNAME}

${OUTPUTDIR}/${OUTBINNAME}: ${OBJDIR}/proclist.o ${OBJDIR}/aswifi.o
	$(GPP) ${SOURCEDIR}/main.cpp ${OBJDIR}/proclist.o ${OBJDIR}/aswifi.o -lpthread -lm -o $@

${OBJDIR}/proclist.o:
	$(GPP) -c ${SOURCEDIR}/proclist.cpp -o $@

${OBJDIR}/aswifi.o:
	$(GPP) -c ${SOURCEDIR}/aswifi.cpp -o $@

clean:
	-rm -f ${OBJDIR}/*.o ${OUTPUTDIR}/${OUTBINNAME}
